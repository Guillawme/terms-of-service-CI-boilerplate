# Terms of Service or Privacy Policy Boilerplate using Latex & Markdown with Gitlab CI

**This projects is based off [curriculumvitae-ci-boilerplate](https://gitlab.com/dimitrieh/curriculumvitae-ci-boilerplate)!**

![preview-ToS](https://gitlab.com/dimitrieh/terms-of-service-CI-boilerplate/raw/master/preview.jpg)

The pdf is reachable by url via gitlab pages, see the following example:  [Terms of Service](http://dimitrieh.gitlab.io/terms-of-service-CI-boilerplate/Terms_of_Service_500px.pdf)

**Run it with**
```
docker run -it -v `pwd`:/source strages/pandoc-docker /bin/bash
```

*Note that after the container is running you have to run the following:*

```
cp fonts/* /usr/share/fonts/
fc-cache -f -v
make
```

## Credits
This repo took [500px terms of service](https://about.500px.com/privacy/) as an example! We are not in any way affiliated with them.

## License

- License: [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
- Linux Libertine O Font License: [GPL](http://www.gnu.org/licenses/gpl.html) and [OFL](http://scripts.sil.org/OFL)
- Source Sans Pro License: [SIL](https://github.com/adobe-fonts/source-sans-pro/blob/master/LICENSE.txt)
